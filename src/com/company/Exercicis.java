package com.company;

public class Exercicis {
    Exercicis[] activitats;

    public Exercicis(Exercicis[] activitats) {
        this.activitats = activitats;
    }

    public Exercicis[] getActivitats() {
        return activitats;
    }

    public void setActivitats(Exercicis[] activitats) {
        this.activitats = activitats;
    }
}
