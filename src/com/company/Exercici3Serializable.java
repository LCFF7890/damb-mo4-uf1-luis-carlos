package com.company;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

public class Exercici3Serializable implements Serializable {
    public static final String FILE_PATH="Exercic.serializable";
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, ClassNotFoundException, TransformerException {
        String sport = "basquet";
        int durancio = 15;
        Exercicis[] activitatArray;
        if(new File(FILE_PATH).exists()){
            Exercicis exercicis = readFromFile();
            activitatArray = exercicis.getActivitats();
        } else {
            activitatArray = new Exercicis[]{};
        }
        Exercicis exercici = new Exercicis(activitatArray);
        activitatArray = addElementToArray(activitatArray, exercici);

        Exercicis exercicis = new Exercicis(activitatArray);
        save(exercicis);
    }

    private static void save(Exercicis exercicis) throws TransformerException {
        String filePath = "EsportTabla.xml";
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource();
        StreamResult result = new StreamResult(new File(filePath));
        transformer.transform(source, result);
    }

    private static Exercicis[] addElementToArray(Exercicis[] activitatArray, Exercicis exercici) {
        return new Exercicis[0];
    }

    private static Exercicis readFromFile() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(FILE_PATH);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Exercicis nom = (Exercicis) objectInputStream.readObject();
        objectInputStream.close();
        return nom;
    }
}