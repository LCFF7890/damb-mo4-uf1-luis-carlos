package com.company;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Exercici3EsportisataTabla {
    public static void main(String[] args) throws TransformerException, ParserConfigurationException, IOException, SAXException {
        String[] elemento;
        File file = new File("EsportTabla.xml");
        boolean fileExeist = file.exists();
        Document doc;
        if (fileExeist) {
            doc = readDom();
        } else {
            doc = crearDoom();
        }
        elemento = pedirIntoducir();
        introducirNuevoValor(doc, elemento);
        save(doc);
    }

    private static Document readDom() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse("EsportTabla.xml");
        Element personElemen = doc.createElement("Person");
        return doc;
    }

    private static void save(Document doc) throws TransformerException {
        String filePath = "EsportTabla.xml";
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(filePath));
        transformer.transform(source, result);
    }
    private static Document crearDoom() throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc= docBuilder.newDocument();
        Element personElemen = doc.createElement("Person");
        doc.appendChild(personElemen);
        return doc;
    }
    private static String[] pedirIntoducir(){
        int i=0;
        String deporte,Duracion;
        String[] retornar = new String[2];
        Scanner lector=new Scanner(System.in);
        System.out.println("Introdusca el esport que ha fer");
        deporte =lector.nextLine();
        retornar[i]=deporte;
        System.out.println("Introdusca la duracion");
        i+=1;
        Duracion=lector.nextLine();
        retornar[i]=Duracion;
        return retornar;
    }
    private static void introducirNuevoValor(Document doc, String[] elemento) {
        int i = 0;
        Element esportElememnt = doc.createElement("Esport");
        doc.getDocumentElement().appendChild(esportElememnt);
        esportElememnt.appendChild(doc.createTextNode(elemento[i]));
        i += 1;
        Element duracioElement = doc.createElement("Duracio");
        doc.getDocumentElement().appendChild(duracioElement);
        duracioElement.appendChild(doc.createTextNode(elemento[i]));
    }
}
