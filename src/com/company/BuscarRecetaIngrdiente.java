package com.company;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Scanner;
public class BuscarRecetaIngrdiente {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
        Document doc=crearDocument();
        doc.getDocumentElement().normalize();
        NodeList recepta = doc.getElementsByTagName("nom");
        String ingrediente= CogerNombre();
        System.out.println("La receta que tiene " + ingrediente + ".");
        imprimir(recepta);
    }
    private static String CogerNombre(){
        Scanner lector = new Scanner(System.in);
        System.out.println("Introdusca el ingrediente que quiera buscar");
        return lector.nextLine();
    }
    private static Document crearDocument() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        return dBuilder.parse("XMLReceptesPractica.xml");
    }
    private static void imprimir(NodeList recepta){
        for (int i = 0; i < recepta.getLength(); i++) {
            Node receta = recepta.item(i);
            Node nomPaerent = receta.getParentNode();
            String comparar = nomPaerent.getNodeName();
            if (comparar.equals("recepta")) {
                Element dificultat = (Element) nomPaerent;
                System.out.println("La recptas es " + receta.getTextContent());
                System.out.println("La dificultat es " + dificultat.getAttribute("dificultat"));
            }
        }
    }
}