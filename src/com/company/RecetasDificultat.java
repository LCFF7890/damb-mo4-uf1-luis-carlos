package com.company;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class RecetasDificultat {

    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse("XMLReceptesPractica.xml");
        doc.getDocumentElement().normalize();
        NodeList dnode=doc.getElementsByTagName("nom");
        for (int i=0;i<dnode.getLength();i++){
            Node nomnode=dnode.item(i);
            Node nomParent=nomnode.getParentNode();
            String nodename=nomParent.getNodeName();
            if (nodename.equals("recepta")) {
                Element receptasNode = (Element) nomParent;
                System.out.println(nomnode.getTextContent());
                System.out.println(receptasNode.getAttribute("dificultat"));
            }
        }
    }
}
