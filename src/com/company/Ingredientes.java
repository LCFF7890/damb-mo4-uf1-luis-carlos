package com.company;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
public class Ingredientes {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        Document doc = document();
        doc.getDocumentElement().normalize();
        NodeList Ingredientes=doc.getElementsByTagName("nom");
        for (int i=0;i<Ingredientes.getLength();i++){
            Node nomnode=Ingredientes.item(i);
            Node nomParent=nomnode.getParentNode();
            String nodename=nomParent.getNodeName();
            if (nodename.equals("ingredient")) {
                System.out.println(nomnode.getTextContent());
            }
        }
    }
    public static Document document() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        return dBuilder.parse("XMLReceptesPractica.xml");
    }
}
